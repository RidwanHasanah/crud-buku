import React, { Component } from 'react';
import axios from 'axios';
import logo from '../logo.svg';
import '../App.css';

class DetailBook extends Component {
    state = {
        data:null
    }

    componentDidMount(){
        console.log(this.props.match.params.id,'<====')
        axios.get('http://localhost:2001/api/data/' + this.props.match.params.id).then(res=>{
            this.setState({
                data: res.data
            })
            console.log(this.state.data)
        })

    }
    render() {
        if(this.state.data == null){
            return(
                <div className="App">
                    <img  className="App-logo" src={logo} alt="logo"/>
                </div>
            )
        }
        return (
            <div>
                <h1>Ini Detail Book</h1>
                <h2>Buku : {this.state.data[0].book}</h2>
                <h2>Title : {this.state.data[0].title}</h2>
            </div>
        );
    }
}

export default DetailBook;