import React, { Component } from 'react';
import axios from 'axios';
import {} from 'reactstrap';
import {Link} from 'react-router-dom';
import logo from '../logo.svg';
import '../App.css';


class Allbooks extends Component {
    state = {
        data:null
    }

    getData = ()=>{
        axios.get("http://localhost:2001/api/data").then((res)=>{
            // console.log(res,'<==================');
            this.setState({
                data:res.data
            });
            console.log(this.state.data)
        })
    }
    deleteData = id =>{
        axios.delete(`http://localhost:2001/api/data/${id}`).then(res=>{
            this.getData();
        })
    }
    componentDidMount(){
        this.getData();
    }

    render() {
        {
            if (this.state.data == null ) {
                return <img src={logo} className="App-logo" alt="logo" />
            }
        }
        return (
            <div className="App">
                 <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                </header>
                <Link to="/addbook"><button className="btn-outline-success">Tambah Buku</button> </Link>

                {
                    this.state.data.map((datum)=>{
                    return(<div>
                        <h3><Link to={`/detailbook/${datum._id}`} >Book : {datum.book}</Link></h3>
                        <h5>Title: {datum.title}</h5>
                        <button onClick={()=>{this.deleteData(datum._id)}} >Delete</button>
                        <Link to={`/updatebook/${datum._id}`} >Update</Link>
                        <hr/>
                        <br/>

                    </div>)
                    })
                }
            </div>
        );
    }
}

export default Allbooks;