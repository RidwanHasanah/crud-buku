import React, { Component } from 'react';
import {} from 'reactstrap';
import {Redirect} from 'react-router-dom';
import axios from 'axios';



class AddBook extends Component {
    state = {
        book: '',
        title: '',
        redirect: false
    }

    postData = ()=>{
        axios.post('http://localhost:2001/api/data',{
            book: this.state.book,
            title: this.state.title
        }).then((res)=>{
            this.setState({
                book: '',
                title: '',
                redirect: true
            })
        })
    }
    
    handleChange = (e)=>{
        this.setState({
            [e.target.name]: e.target.value
        });
    }


    render() {
        return (
            <div className='container'>
                <div className="row">
                    <div className="col col-lg-8 col-md-8">
                        <div className="form-group col-md-6">
                            <label>Book</label>
                            <input onChange={this.handleChange} value={this.state.book} type="text" className="form-control" name="book" placeholder="IPA" />
                        </div>
                        <div className="form-group col-md-6">
                            <label>Title</label>
                            <input onChange={this.handleChange} value={this.state.title} type="text" className="form-control" name="title" placeholder="Biologi" />
                        </div>
                        <button onClick={()=>{this.postData()}} className="btn-outline-primary">Tammbah</button>
                        {this.state.redirect ?<Redirect to="/" />:'' }
                    </div>
                </div>
                
            </div>
        );
    }
}

export default AddBook;