import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import axios from 'axios';

class UpdataBook extends Component {
    state ={
        book: '',
        title: '',
        redirect: false
    }

    componentDidMount(){
        axios.get('http://localhost:2001/api/data/'+this.props.match.params.id).then(res=>{
            this.setState({
                book : res.data[0].book,
                title: res.data[0].title
            })
        })
    }

    putData = id =>{
        axios.put('http://localhost:2001/api/data/'+id,{
            book : this.state.book,
            title: this.state.title
        }).then(res=>{
            this.setState({
                book: '',
                title: '',
                redirect: true
            })
        })
    }

    handleChange = e =>{
        this.setState({
            [e.target.name] : e.target.value
        });
    }


    render() {
        return (
            <div>
                <label>Buku</label>
                <input onChange={this.handleChange} type="text" value={this.state.book} name="book"/>
                <label>Title</label>
                <input onChange={this.handleChange} type="text" value={this.state.title} name="title"/>
                <button onClick={()=>{this.putData(this.props.match.params.id)}} >Ubah</button>
                {this.state.redirect ? <Redirect to="/" />:''}
            </div>
        );
    }
}

export default UpdataBook;