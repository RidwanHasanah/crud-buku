import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Allbooks from './books/Allbooks';
import AddBook from './books//AddBook';
import DetailBook from './books/DetailBook';
import UpdateBook from './books/UpdataBook';
import {BrowserRouter, Switch, Route} from 'react-router-dom';


class App extends Component {
  render() {
    return (
      <div >
        <BrowserRouter>
          <Switch>
            <Route path="/" exact component={Allbooks} />
            <Route path="/addbook" component={AddBook}/>
            <Route path="/detailbook/:id" component={DetailBook} />
            <Route path="/updatebook/:id" component={UpdateBook} />
          </Switch>
        </BrowserRouter>        
      </div>
    );
  }
}

export default App;
